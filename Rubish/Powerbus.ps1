﻿#Add-Type -Path .\Microsoft.ServiceBus.dll
#Add-Type -Path .\Microsoft.WindowsAzure.Configuration.dll

#$PSDefaultParameterValues.Add("*-Bus*:ConnectionString","")
#$PSDefaultParameterValues.Add("*-Bus*:QueueNameString","")
#[string] $Script:AzureConnectionString = "Endpoint=sb://spvtest.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=mbp72DG/CGwZSBp88e4uJDff6We6dVu2ZKM1zxkK9So="
#[string] $Script:QueueName = "spvqueue"
[string[]] $script:MessageProperties = @()
[string[]] $Script:Assemblies =  @(".\Microsoft.ServiceBus.dll",".\Microsoft.WindowsAzure.Configuration.dll")
$Script:Properties = New-Object pscustomobject

function Add-BusProperty
{
[cmdletbinding()]
Param(
    [string[]] $Name
)
    $f = $MyInvocation.MyCommand.Name
    Write-Verbose -Message "$f - START"
    
    if(-not $Script:Properties)
    {
        Write-Verbose -Message "$F -  Creating new object"
        $Script:Properties = New-Object pscustomobject
    }

    Write-Verbose -Message "$f -  Adding properties"

    foreach ($Prop in $Name)
    {
        Write-Verbose -Message "$f -  Adding prop $prop"
        $Script:Properties | Add-Member -MemberType NoteProperty -Name $Prop -Value "k"
    }
    Write-Verbose -Message "$f - END"
}

Function Verify-AssemblyLoaded
{
[cmdletbinding()]
Param(
    [string[]] $AssemblyPath
)
    $f = $MyInvocation.MyCommand.Name
    Write-Verbose -Message "$f - Start"

    $Assembly = $null

    foreach($assembly in $Assemblies)
    {
        Write-Verbose -Message "$f -  Processing assemly $Assembly"
        $path = $Assembly | Resolve-Path
        $Directory = $path.path

        Write-Verbose -Message "$f -  Path is $($path.path)"

        $Directory = $Directory | Split-Path
        $assemblyName = $path.path.replace("$Directory\","").replace(".dll","")

        Write-Verbose "$f -  Checking if assembly is loaded"

        if(-not (Get-Assembly -Name $assemblyName))
        {
            Write-Verbose -Message "$f -  Loading assembly $assemblyName.dll"
            Load-Assembly -Path $path.path
        }
    }
    
    Write-Verbose -Message "$f - End"
}

function Load-Assembly
{
Param(
    [Parameter(Mandatory=$true)]
    [string[]] $Path
)
    $f = $MyInvocation.MyCommand.Name
    Write-Verbose -Message "$f - Start"

    foreach($file in $Path)
    {
        if(Test-Path -Path $file)
        {
            Write-Verbose -Message "$f -  Loading assembly $file"
            Add-Type -Path $file -ErrorAction Stop
        }
        else
        {
            Write-Warning -Message "$f -  Unable to find assembly $file"
        }
    }
    
    Write-Verbose -Message "$f - End"
}

function Send-APIBusMessage
{
[CmdletBinding()]
[OutPutType([bool])]
Param(
    [Parameter(Mandatory)]
    [Microsoft.ServiceBus.Messaging.QueueClient]
    $QueueClient
    ,
    [Parameter(Mandatory)]
    [Microsoft.ServiceBus.Messaging.BrokeredMessage]
    $Message
)
    [bool] $ReturnValue = $false

    $f = $MyInvocation.MyCommand.Name
    Write-Verbose -Message "$f - Start"

    try
    {
        Write-Verbose -Message "$f -  Trying to send"
        $QueueClient.Send($Message)
        Write-Verbose -Message "$f -  Busmessage sent"
        $ReturnValue = $true
    }
    catch [Microsoft.ServiceBus.Messaging.MessagingException]
    {
        Write-Verbose -Message "$f -  MessagingException"
        if(-not $_.Exception.IsTransient)
        {
            Write-Error -Exception $_.Exception -Message $_.Exception.Message
            throw "Error sending message to Azure for Queue $QueueName"
        }
        else
        {
            [string]$Msg = "$f -  Error while sending (IsTransient). Please retry"            
        }
    }
    catch
    {
        Write-Verbose -Message "$f -  General exception"
        Write-Error -Exception $_.Exception -Message $_.Exception.Message
    }

    Write-Verbose -Message "$f - End"
    $ReturnValue
}

function Send-BusMessage
{
[CmdletBinding()]
[outputtype([bool])]
Param(
    [string] $MessageBody
    ,
    [string] $Label
    ,
    [PSCustomObject] $PayloadProperties
    ,
    [int] $MessageID
    ,
    [int] $RetryCount
    ,
    [string] $ConnectionString
    ,
    [string] $QueueNameString
    )

    $f = $MyInvocation.MyCommand.Name
    Write-Verbose -Message "$f - Start"

    Write-Verbose -Message "$f -  Verify default parameters"

    if($ConnectionString -or $QueueNameString)
    {
        throw "ConnectionString and/or QueueNameString is not specified"
    }

    Verify-AssemblyLoaded -AssemblyPath $Script:Assemblies

    [bool] $SendSuccess = $False
    try
    {
        Write-Verbose -Message "$f -  Creating client"
        [Microsoft.ServiceBus.Messaging.QueueClient]$QueueClient = [Microsoft.ServiceBus.Messaging.QueueClient]::CreateFromConnectionString($ConnectionString, $QueueNameString)
        
        [Microsoft.ServiceBus.Messaging.BrokeredMessage] $Message = New-Object -TypeName Microsoft.ServiceBus.Messaging.BrokeredMessage -ArgumentList @(,"$MessageBody")
        $Message.Label = "$Label"

        if($MessageID)
        {
            $message.MessageId = $MessageID
        }

        Write-Verbose -Message "$f -  Checking for payload properties"
        if($PayloadProperties)
        {
            $Props = ($PayloadProperties |Get-Member -MemberType NoteProperty).Name
            foreach($prop in $Props)
            {
                Write-Verbose -Message "$f -  Adding property $prop with value '$($PayloadProperties.$prop)'"
                $Message.Properties.Add($prop,$PayloadProperties.$prop)
            }
        }

        #$Message
        Write-Verbose -Message "$f -  Calling Send-APIBusMessage to send message"
        #$QueueClient.Send($message)
        $SendSuccess = Send-APIBusMessage -QueueClient $QueueClient -Message $Message

    }
    catch
    {
        Write-Error -Exception $_.Exception -Message $_.Exception.Message
    }

    Write-Verbose -Message "$f - End"
    $SendSuccess
}

function Get-APIBusMessage
{
[CmdletBinding()]
[OutPutType([Microsoft.ServiceBus.Messaging.BrokeredMessage])]
Param(
    [Parameter(Mandatory)]
    [Microsoft.ServiceBus.Messaging.QueueClient]
    $QueueClient
    ,
    [switch] $DeQueue
    ,
    [System.TimeSpan] $WaitTimeSpan
)
    
}

function Get-BusMessage
{
[CmdletBinding()]
[OutPutType([Microsoft.ServiceBus.Messaging.BrokeredMessage])]
Param(
    [switch] $DeQueue
    ,
    [string] $ConnectionString
    ,
    [string] $QueueNameString
    )
    $f = $MyInvocation.MyCommand.Name

    Write-Verbose -Message "$f - Start"

    Verify-AssemblyLoaded -AssemblyPath $Script:Assemblies

    [Microsoft.ServiceBus.Messaging.QueueClient]$QueueClient = [Microsoft.ServiceBus.Messaging.QueueClient]::CreateFromConnectionString($Script:AzureConnectionString, $Script:QueueName)

    [Microsoft.ServiceBus.Messaging.BrokeredMessage] $Message = $null

    if($DeQueue)
    {        
        Write-Verbose -Message "$f -  Running dequeue"
        $Message = $QueueClient.Receive([System.TimeSpan]::FromSeconds(5))
        if($Message)
        {
            $Message
            [void]$Message.Complete()
        }
        else
        {
            break
        }
    }
    else
    {
        Write-Verbose -Message "$f -  Running peek"
        $Message = $QueueClient.Peek()
        $Message
    }

     Write-Verbose -Message "$f - End"
     
}

function Get-BusDefaults
{
[cmdletbinding()]
Param()

    $PSDefaultParameterValues
}

function Set-BusDefaults
{
[cmdletbinding()]
Param(
    [string]$BusConnectionstring
    ,
    [string]$QueueName
)

    foreach($assembly in $Script:Assemblies)
    {
        if(-not (Verify-AssemblyLoaded -AssemblyPath (Resolve-Path -Path $assembly)))
        {
            Load-Assembly -Path (Resolve-Path -Path $assembly)
        }
    }

    if($BusConnectionstring)
    {
        if($PSDefaultParameterValues.ContainsKey("*-Bus*:ConnectionString"))
        {
            $PSDefaultParameterValues["*-Bus*:ConnectionString"] = $BusConnectionstring
        }
        else
        {
            $PSDefaultParameterValues.Add("*-Bus*:ConnectionString",$BusConnectionstring)
        }
    }

    if($QueueName)
    {
        if($PSDefaultParameterValues.ContainsKey("*-Bus*:QueueNameString"))
        {
            $PSDefaultParameterValues["*-Bus*:QueueNameString"] = $QueueName
        }
        else
        {
            $PSDefaultParameterValues.Add("*-Bus*:QueueNameString",$QueueName)
        }
    }
}

function Test-BusDefaults
{
[cmdletbinding()]
Param(
    [string] $ConnectionString
    ,
    [string] $QueueNameString
)
    $out = New-Object psobject -Property @{ConnectionString = $ConnectionString;QueueNameString = $QueueNameString}
    $out
}

function Get-BusBrokeredMessage
{
    Verify-AssemblyLoaded -AssemblyPath $Script:Assemblies

    [Microsoft.ServiceBus.Messaging.BrokeredMessage] $Message = New-Object Microsoft.ServiceBus.Messaging.BrokeredMessage
    $Message
}

function Get-Assembly
{
<#
.SYNOPSIS
    Get .net assemblies loaded in your session
.DESCRIPTION
    List assemblies loaded in the current session. Wildcards are supported. 
    Requires powershell version 2
.PARAMETER Name
    Name of the assembly to look for. Supports wildcards
.EXAMPLE
    Get-Assembly

    Returns all assemblies loaded in the current session
.EXAMPLE
    Get-Assembly -Name *ServiceBus*

    Returns loaded assemblies which contains ServiceBus
  
.NOTES 
     SMART
     AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
#>
[cmdletbinding()]
Param(
    [String] $Name
)
    $f = $MyInvocation.MyCommand.Name 
    Write-Verbose -Message "$f - Start"

    if($name)
    {
        $dlls = [System.AppDomain]::CurrentDomain.GetAssemblies() | where {$_.FullName -like "$name"}
    }
    else
    {
        $dlls = [System.AppDomain]::CurrentDomain.GetAssemblies()
    }

    if($dlls)
    {
        foreach ($dll in $dlls)
        {
            $Assembly = "" | Select-Object FullName, Version, Culture, PublicKeyToken
            $DllArray = $dll -split ","
            if($DllArray.Count -eq 4)
            {
                Write-Verbose -Message "$f -  Building custom object"
                $Assembly.Fullname = $DllArray[0]
                $Assembly.Version = $DllArray[1].Replace("Version=","")
                $Assembly.Culture = $DllArray[2].Replace("Culture=","")
                $Assembly.PublicKeyToken = $DllArray[3].Replace("PublicKeyToken=","")
                $Assembly
            }
            else
            {
                Write-Verbose -Message "$f-  Array length/count is NOT 4"
            }
        }
    }
    else
    {
        Write-Verbose -Message "$f -  nothing found"
    }
    Write-Verbose -Message "$f - End"
}


#Export-ModuleMember -Function *