﻿function Load-Assembly {
Param(
    [string[]] $Path
)
    $f = $MyInvocation.MyCommand.Name
    Write-Verbose -Message "$f - Start"

    if(-not $Path)
    {
        $Path = $Script:Assemblies | foreach {Resolve-Path}
    }

    foreach($file in $path)
    {
        if(Test-Path -Path $file)
        {
            Write-Verbose -Message "$f -  Loading assembly $file"
            Add-Type -Path $file -ErrorAction Stop
        }
        else
        {
            Write-Verbose -Message "$f -  throwing exception file not found"
            Write-Error "assembly not found" -Exception System.IO.FileNotFoundException
        }
    }

    Write-Verbose -Message "$f - End"
}
