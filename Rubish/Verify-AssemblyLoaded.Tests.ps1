﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.", ".")
. "$here\$sut"

function Get-Assembly {}

Describe "Verify-AssemblyLoaded" {
    
    Context "parameter validation" {
        it "blank should throw" {
            { Verify-AssemblyLoaded "" } | Should throw
        }
        it "null should throw" {
            { Verify-AssemblyLoaded $null } | Should throw
        }
    }

    Context "assembly is loaded" {
        Mock Get-Assembly { $true }
        It "should return true" {
            Verify-AssemblyLoaded -AssemblyPath ".\" | Should Be $true
        }
    }
    
    Context "assembly is not loaded" {
        Mock Get-Assembly { $false }
        It "should return false" {
            Verify-AssemblyLoaded -AssemblyPath ".\" | Should Be $false
        }
    }
}
